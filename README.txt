This is the repository for my Bachelor Thesis based on the previous work of Claudio Cannizzaro.
His work can be found under: https://gitlab.ethz.ch/claudioc/scientific-visualisation-of-high-resolution-climate-models 

The purpose of this program is to generate plots from the output of COSMO models.
It does this by reading in .nc files directly.

The main components of the program are the Visualization.py, VisParam.py and a JSON configuration file.
I recommend to set up a new virtual environment (e.g. with conda) for python to run. During the setup of the virtual
environment, it is recommended to use the environment.yml file to install the packages needed for the program.

If you want to install the packages manually, make sure you have those listed below:
- xarray
- matplotlib
- django
- numpy
- h5netcdf
- dill
- cartopy

Running the program functions in the following way:
python visualize_climate_model.py path_to_CONFIG_file.json

The options for the JSON configuration file are described in the JSON_configuration_README.txt file.
In the VisParam file, colormaps for the variables and other parameters can be adapted.

I assume that the nc files are named in the following way:
    "lffd" + YYYYmmddHHMMSS + ".nc"

and the constant nc file:
    "lffd" + YYYYmmddHHMMSS + "c.nc"

to generate the video run this command in the folder with the generated the pictures
ffmpeg -framerate 20  -pattern_type glob -i '*.jpg' \
  -c:v libx264 -pix_fmt yuv420p -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" NAMEOFVIDEO.mp4