Options for the JSON configuration file:

!! IMPORTANT !! use either TQC+TQI or CC for cloud visualization

"title" : string containing title or null for no title,

"num_processes" : amount of parallel processes to spawn, integer or null to automatically detect available cores,
                    takes take max(integer, available cores)

"use_native_resolution": true to infer appropriate resolution from data (nc file), false for predefined resolution, has
                    a minimum size to ensure readability of title, time and date

"draw_grid_lines": true/false, toggles a labeled longitude latitude grid

"vis_duration": string of format "D (days) hh:mm:ss", duration of the simulation data to be visualized

"vis_start_date": string of format "YYYY-MM-DD hh:mm:ss", ISO 8601 & PostgreSQL’s day-time interval formats also allowed
                    starting time of the visualisation

"plotting_time_step": string of format "(D days) hh:mm:ss", declares how often a new frame is plotted, is at least as
                    big as the smallest data time step

"outputPath": string with path to the folder where the pictures will be stored

"enable_plotting": true/false for each variable, turn on CC for the visualization of clouds with cloud fractions (CLCH,
                    CLCM, CLCH) weightings of the fractions can be adapted in the VisParam.py file.
                    Turn on TQI+TQC for cloud visualization through the cloud water and cloud ice content
                    if W_SO is turned on the earth's surface will be coloured, for more detailed information on the specific
                    variables refer to C. Cannizzaros thesis (available at https://zenodo.org/record/7239053)

"const_path": string with path to constant data which is used to plot the earth's surface,

"paths": string with path to location of the nc file or null for each variable,

"data_time_stepping": string of the format "(D days) hh:mm:ss" or null for each variable, time steps at which variable
                        data is available, must coincide with the available data
